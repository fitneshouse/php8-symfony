FROM php:8.1-fpm-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV TZ ${TZ:-"Europe/Moscow"}
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS ${PHP_OPCACHE_VALIDATE_TIMESTAMPS:-0}
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES ${PHP_OPCACHE_MAX_ACCELERATED_FILES:-10000}
ENV PHP_OPCACHE_MEMORY_CONSUMPTION ${PHP_OPCACHE_MEMORY_CONSUMPTION:-192}
ENV PHP_OPCACHE_MAX_WASTED_PERCENTAGE ${PHP_OPCACHE_MAX_WASTED_PERCENTAGE:-10}

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

COPY symfony-cli_5.4.8_x86_64.apk /srv/symfony-cli_5.4.8_x86_64.apk

RUN set -eux; apk add --no-cache tzdata git mc wget curl shadow bash yarn nodejs icu-data-full fcgi

RUN set -eux; apk add --no-cache --allow-untrusted /srv/symfony-cli_5.4.8_x86_64.apk

RUN set -eux; ln -snf /usr/share/zoneinfo/$TZ /etc/localtime; echo $TZ > /etc/timezone;

RUN set -eux; \
    chmod +x /usr/local/bin/install-php-extensions; \
    install-php-extensions gd intl zip mysqli pdo_mysql calendar exif imagick memcached pcntl sockets tidy xsl uuid gettext soap opcache bcmath; \
    apk add --update --no-cache --virtual .phpize-deps $PHPIZE_DEPS

COPY profile/aliases.sh /etc/profile.d/aliases.sh

RUN set -eux; \
    rm -rf /srv/symfony-cli_5.4.8_x86_64.apk; \
    rm -rf /usr/share/php; \
    rm -rf /tmp/*; \
    rm -rf /var/cache/apk/*; \
    apk del  .phpize-deps

COPY --from=composer:2.5 /usr/bin/composer /usr/local/bin/composer

COPY conf.d/php-extend.ini /usr/local/etc/php/conf.d/php-extend.ini
COPY conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

COPY healthcheck/fpm-docker-healthcheck.conf /usr/local/etc/php-fpm.d/fpm-docker-healthcheck.conf
COPY healthcheck/docker-healthcheck /usr/local/bin/docker-healthcheck
RUN  chmod +x /usr/local/bin/docker-healthcheck
HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD ["docker-healthcheck"]

EXPOSE 9000

WORKDIR "/srv/app"

