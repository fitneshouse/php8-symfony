help:
	@echo "Available command: build build-dev start-local start-remote connect dive security lint dockle"

build:
	docker build --pull --tag 'local-php8-symfony:latest' .

build-dev:
	docker build --pull --tag 'local-php8-symfony-dev:latest' -f Dockerfile-dev  .

lint:
	docker run --rm -i  -v /var/run/docker.sock:/var/run/docker.sock hadolint/hadolint < Dockerfile

start-local:
	docker run -it --rm --name local-test2 local-php8-symfony:latest

start-remote:
	docker run -it --rm --name local-test2 registry.gitlab.com/fitneshouse/php8-symfony:latest

connect:
	docker exec -it local-test2 /bin/sh

dive:
	docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock  wagoodman/dive:latest  local-php8-symfony:latest

security:
	docker run --rm -v /var/run/docker.sock:/var/run/docker.sock aquasec/trivy image local-php8-symfony:latest

dockle:
	docker run --rm -v /var/run/docker.sock:/var/run/docker.sock goodwithtech/dockle:latest local-php8-symfony:latest

